# Webpack Frontend Starterkit

[![Dependabot badge](https://flat.badgen.net/dependabot/wbkd/webpack-starter?icon=dependabot)](https://dependabot.com/)

A lightweight foundation for your next webpack based frontend project.

### Installation

```
npm install
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```

### Features:

- ES6 Support via [babel](https://babeljs.io/) (v7)
- JavaScript Linting via [eslint-loader](https://github.com/MoOx/eslint-loader)
- SASS Support via [sass-loader](https://github.com/jtangelder/sass-loader)
- Autoprefixing of browserspecific CSS rules via [postcss](https://postcss.org/) and [autoprefixer](https://github.com/postcss/autoprefixer)
- Style Linting via [stylelint](https://stylelint.io/)

When you run `npm run build` we use the [mini-css-extract-plugin](https://github.com/webpack-contrib/mini-css-extract-plugin) to move the css to a separate file. The css file gets included in the head of the `index.html`.


// краткое описание

Сферу необходимо делить на 8 одинаковых частей, назвать их просто по их номеру он 0 до 7. И для каждой необходимо создать версию с меньшим разрешением и в названии добавить "_small". Вся логика для сферы находится в файле `srs/scripts/Sphere/Sphere.js` (Размер, качество, кол-во частей, логику названия каждой части и тд.);
Изображения размещать в папке "./public/images/[image_name]" (по аналогии с существующими);
Для создания сферы нужно только передать путь к папке `new Sphere( { path: './public/images/1/' } )`.

Изображения с низким разрешением загружаются сразу после инициализации сферы.
Метод `setActive()` у сферы запускает загрузку изображений высокого разрешения.
На сцену добавлять свойство `mainContainer` у сферы.
